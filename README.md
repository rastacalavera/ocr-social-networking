# OCR Social Networking

Curious about "Federated Networks"? Looking for alternatives to Twitter, YouTube or Slack?

This project aims to inform community members about Open Social Networks.

Check out the Wiki for Specific Topics

[Welcome to the Fediverse](https://gitlab.com/rastacalavera/ocr-social-networking/-/wikis/Welcome-to-the-Fediverse)